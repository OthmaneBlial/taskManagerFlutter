import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:helloworld/task.dart';
import 'package:helloworld/home_screen.dart';

class TaskDetailScreen extends StatefulWidget {
  final Task task;
  final Function(Task) onTaskUpdated;
  final Function(Task) onTaskDeleted;

  TaskDetailScreen(
      {required this.task,
      required this.onTaskUpdated,
      required this.onTaskDeleted});

  @override
  _TaskDetailScreenState createState() => _TaskDetailScreenState();
}

class _TaskDetailScreenState extends State<TaskDetailScreen> {
  late TextEditingController _nameController;
  late TextEditingController _descriptionController;
  late DateTime _dueDate;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: widget.task.name);
    _descriptionController =
        TextEditingController(text: widget.task.description);
    _dueDate = widget.task.dueDate;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  Future<void> _updateTask() async {
    final task = Task(
      id: widget.task.id,
      name: _nameController.text,
      dueDate: _dueDate,
      description: _descriptionController.text,
    );

    final url = Uri.parse('http://localhost:5000/tasks/${task.id}');
    final queryParameters = {
      'name': task.name,
      'dueDate': task.dueDate.toIso8601String(),
      'description': task.description,
    };

    final response = await http.put(
      url.replace(queryParameters: queryParameters),
    );

    if (response.statusCode == 200) {
      widget.onTaskUpdated(task);
      Navigator.pop(context);
    } else {
      throw Exception('Failed to update task');
    }
  }

  Future<void> _deleteTask() async {
    try {
      await http
          .delete(Uri.parse('http://localhost:5000/tasks/${widget.task.id}'));
      Navigator.of(context).pop();

      // Refresh the task list after deleting a task
      setState(() {});
    } catch (error) {
      throw Exception('Failed to delete task');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.task.name),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              decoration: InputDecoration(
                labelText: 'Name',
              ),
              controller: _nameController,
            ),
            SizedBox(height: 16.0),
            TextField(
              decoration: InputDecoration(
                labelText: 'Description',
              ),
              controller: _descriptionController,
              maxLines: null,
            ),
            SizedBox(height: 16.0),
            InkWell(
              onTap: () async {
                final newDate = await showDatePicker(
                  context: context,
                  initialDate: _dueDate,
                  firstDate: DateTime.now(),
                  lastDate: DateTime(2100),
                );
                if (newDate != null) {
                  setState(() {
                    _dueDate = newDate;
                  });
                }
              },
              child: InputDecorator(
                decoration: InputDecoration(
                  labelText: 'Due Date',
                ),
                child: Row(
                  children: [
                    Text(_dueDate.toString()),
                    Icon(Icons.arrow_drop_down),
                  ],
                ),
              ),
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: _updateTask,
              child: Text('Save'),
            ),
            SizedBox(height: 16.0),
            TextButton(
              onPressed: _deleteTask,
              style: TextButton.styleFrom(
                primary: Colors.red,
              ),
              child: Text('Delete'),
            ),
          ],
        ),
      ),
    );
  }
}
