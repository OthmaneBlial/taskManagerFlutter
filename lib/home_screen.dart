import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:helloworld/task.dart';
import 'package:helloworld/task_detail_screen.dart';
import 'package:helloworld/create_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Task> _tasks = [];

  @override
  void initState() {
    super.initState();
    _fetchTasks();
  }

  Future<void> _fetchTasks() async {
    final response = await http.get(Uri.parse('http://localhost:5000/tasks'));

    if (response.statusCode == 200) {
      final List<dynamic> jsonTasks = jsonDecode(response.body);
      final List<Task> tasks =
          jsonTasks.map((json) => Task.fromJson(json)).toList();
      setState(() {
        _tasks = tasks;
      });
    } else {
      throw Exception('Failed to fetch tasks');
    }
  }

  void _updateTask(Task task) async {
    final response = await http.put(
      Uri.parse('http://localhost:5000/tasks/${task.id}'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(task.toJson()),
    );

    if (response.statusCode == 200) {
      setState(() {
        int index = _tasks.indexWhere((t) => t.id == task.id);
        _tasks[index] = task;
      });
    } else {
      throw Exception('Failed to update task');
    }
  }

  void _deleteTask(Task task) async {
    final response =
        await http.delete(Uri.parse('http://localhost:5000/tasks/${task.id}'));

    if (response.statusCode == 204) {
      setState(() {
        _tasks.remove(task);
      });
    } else {
      throw Exception('Failed to delete task');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Task Manager'),
      ),
      body: ListView.builder(
        itemCount: _tasks.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: ListTile(
              title: Text(_tasks[index].name),
              subtitle: Text('Due ${_tasks[index].dueDate.toString()}'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TaskDetailScreen(
                      task: _tasks[index],
                      onTaskUpdated: _updateTask,
                      onTaskDeleted: _deleteTask,
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CreateScreen(),
            ),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
