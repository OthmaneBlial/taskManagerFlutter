import 'package:flutter/material.dart'; // Flutter widgets and material design library
import 'package:helloworld/splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Your App',
      home: SplashScreen(),
    );
  }
}
