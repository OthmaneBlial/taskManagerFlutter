import 'package:uuid/uuid.dart';

class Task {
  final String? id;
  final String name;
  final DateTime dueDate;
  final String description;

  Task({
    String? id,
    required this.name,
    required this.dueDate,
    required this.description,
  }) : id = id ?? Uuid().v4();

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: json['id'],
      name: json['name'],
      dueDate: DateTime.parse(json['dueDate']),
      description: json['description'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['dueDate'] = this.dueDate.toIso8601String();
    data['description'] = this.description;
    return data;
  }
}
